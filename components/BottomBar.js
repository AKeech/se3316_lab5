import React from 'react'
import {Navbar,NavItem,MenuItem, NavDropdown,Nav,Button,Label} from 'react-bootstrap'
import {Link} from 'react-router'
import FontIcon from 'material-ui/FontIcon';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
import IconLocationOn from 'material-ui/svg-icons/communication/location-on';

const recentsIcon = <FontIcon className="material-icons">restore</FontIcon>;
const favoritesIcon = <FontIcon className="material-icons">favorite</FontIcon>;
const nearbyIcon = <IconLocationOn />;


  export default React.createClass({
    getInitialState() {
      return {
        selectedIndex: 0
      };
    },


    render() {
      return (
        <Navbar fixedBottom>

    <Nav>

      <NavItem>
        <Link to='/privacy_policy' style={{textDecoration: 'none', color: '#757575'}}>
          Privacy Policy
        </Link>
      </NavItem>
      <NavItem>
        <Link to='/dmca' style={{textDecoration: 'none', color: '#757575'}}>
          DMCA Policy
        </Link>
      </NavItem>
    </Nav>
      </Navbar>
      );
    }
  });
